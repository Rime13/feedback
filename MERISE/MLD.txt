Table utilisateurs {
  id int [pk]
  prenom varchar
  email varchar
}

Table retrospectives {
  id int [pk]
  utilisateur_id int 
  titre varchar
  description varchar
  date_creation date 
}

Table participent {
  utilisateur_id int [pk]
  retrospective_id int [pk]
}

Table feedbacks {
  id int [pk]
  retrospective_id int
  date_ajout int
  contenus text
}

Ref : utilisateurs.id > retrospectives.utilisateur_id
Ref : utilisateurs.id > participent.utilisateur_id
Ref : retrospectives.id > participent.retrospective_id
Ref : retrospectives.id > feedbacks.retrospective_id
