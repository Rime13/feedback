CREATE DATABASE retro;

CREATE USER if NOT EXISTS 'userRetro'@'localhost' IDENTIFIED BY 'password';

GRANT ALL PRIVILEGES ON retro.* TO 'userRetro'@'localhost';

FLUSH PRIVILEGES;

USE retro;

CREATE TABLE `utilisateurs` (
  `id` int PRIMARY KEY,
  `prenom` varchar(255),
  `mail` varchar(255)
);

CREATE TABLE `retrospectives` (
  `id` int PRIMARY KEY,
  `utilisateur_id` int,
  `titre` varchar(255),
  `description` varchar(255),
  `date_debut` date,
  `date_fin` date,
  FOREIGN KEY (utilisateur_id) REFERENCES utilisateurs(id)
);

CREATE TABLE `participants` (
  `utilisateur_id` int,
  `retrospective_id` int,
  PRIMARY KEY (`utilisateur_id`, `retrospective_id`),
  FOREIGN KEY (utilisateur_id) REFERENCES utilisateurs(id),
  FOREIGN KEY (retrospective_id) REFERENCES retrospectives(id)
);

CREATE TABLE `feedbacks` (
  `id` int PRIMARY KEY,
  `retrospective_id` int,
  `contenus` text,
  `aime` boolean,
  FOREIGN KEY (retrospective_id) REFERENCES retrospectives(id)

);



