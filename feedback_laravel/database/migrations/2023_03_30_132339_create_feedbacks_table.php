<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('feedbacks', function (Blueprint $table) {
            $table->id();
            
            $table->uuid('retrospective_id')->nullable(false);
            $table->foreign('retrospective_id')->references('id')->on('retrospectives')->constrained();
            $table->text('titre');
            $table->text('contenus');
            $table->boolean('aime');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('feedbacks');
    }
};
