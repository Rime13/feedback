<?php

namespace Database\Factories;
use App\Models\User;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Retrospective>
 */
class RetrospectiveFactory extends Factory
{
    /**
     * Définit les données que la table Retrospective doit contenir
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $userIds = User::pluck('id')->toArray();
        return [
            // 'id' =>fake()->uuid(),
            'user_id' =>fake()->randomELement($userIds),
            'titre' =>fake()->sentence(),
            'description' =>fake()->text(),
            'date_debut' =>now(),
            'date_fin' =>fake()->date(),
            
        ];
    }
}
