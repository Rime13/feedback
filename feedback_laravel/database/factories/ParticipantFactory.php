<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;
use App\Models\Retrospective;



/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Participe>
 */
class ParticipantFactory extends Factory
{

    /**
     * Definit le type de données que la table participe doit contenir
     *
     * @return array
     */
    public function definition(): array
    {   
        //crée un tableau de données avec les id
        //Pluck recupere les données et le mets dans un tableau

        $userIds = User::pluck('id')->toArray();
        $retrospectiveIds = Retrospective::pluck('id')->toArray();
        //définit les types délément a créer
        return [
            //
            'user_id' => fake()->randomELement($userIds),
            'retrospective_id' => fake()->randomELement($retrospectiveIds),

        ];
    }
}
