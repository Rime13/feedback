<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Retrospective;


/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Feedback>
 */
class FeedbackFactory extends Factory
{

    /**
     * Définit le type de données que la table feedback doit contenir
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        //Pluck recupere les données et le mets dans un tableau
        $retrospectiveIds = Retrospective::pluck('id')->toArray();

        return [
            'retrospective_id' => fake()->randomELement($retrospectiveIds),
            'titre' => fake()->text(),
            'contenus' => fake()->text(),
            'aime' => fake()->boolean()

        ];
    }
}
