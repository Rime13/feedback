<?php

namespace App\Http\Controllers;

use App\Models\Retrospective;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;

class RetrospectiveController extends Controller
{
    /**
     * Valide les données
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $datas = $request->validate([
            'titre' => ['required', 'string'],
            'description' => ['required', 'string'],
            'date_debut' => ['required', 'date'],
            'date_fin' => ['required', 'date'],
        ]);
        // Retrospective::create($datas);
        /**
         * 
         */
        $r = new Retrospective();
        $r->titre = $datas["titre"];
        $r->description = $datas["description"];
        $r->user_id = Auth::user()->id;
        $r->date_debut = $datas["date_debut"];
        $r->date_fin = $datas["date_fin"];
        $r->save();
        // Registered::dispatch($user);
        return redirect()->intended(route('retrospective.create'));
    }



    /**
     * Crée une rétro en BDD
     *
     * @param Request $request
     * @return void
     */
    public function create()
    {

        return Inertia::render('Retro/Create');
    }

    public function index()
    {

        $retrospectives = Retrospective::all();
        return Inertia::render('Retro/Index', ['retrospectives' => $retrospectives]);
        // return Inertia::render('Retro/Index');
    }

    public function show($id)
    {
        $retrospective = Retrospective::find($id);
        $retrospective->load('feedback');
        return Inertia::render('Retro/Show', ['retrospective' => $retrospective]);
    }
};
