<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $datas = $request->validate([
            'retrospective_id' => ['required','string'],
            'titre' => ['required', 'string'],
            'contenus' => ['required', 'string'],
            'aime' => ['required', 'boolean'],
        ]);
        // Retrospective::create($datas);
        /**
         * 
         */
        $r = new Feedback();
        $r ->retrospective_id = $datas ["retrospective_id"];
        $r ->titre = $datas["titre"];
        $r->contenus = $datas["contenus"];
        $r->aime = $datas["aime"];
        $r->save();
        // Registered::dispatch($user);
        return back();
    }

    /**
     * Affiche le feedback
     */
    public function show(Feedback $feedback)
    {
        //
    }

    /**
     * Inutile
     */
    public function edit(Feedback $feedback)
    {
        //
    }

    /**
     * Inutile
     */
    public function update(Request $request, Feedback $feedback)
    {
        //
    }

    /**
     * Supprime le feedback
     */
    public function destroy(Feedback $feedback)
    {
        //
    }
}
