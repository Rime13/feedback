<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


class Feedback extends Model
{
    use HasFactory;
    //surcharge le nom de la table de feedback en feedbacks
    public $table = 'feedbacks';
    //Permet de ne pas prendre en compte les champs created_at et updated_at

    protected $hidden = ['created_at', 'updated_at'];
    public $timestamps = false;

    //Relation entre les différentes tables

    public function retrospective(): BelongsTo
{
    return $this->belongsTo(Retrospective::class, 'retrospective_id');
}
}
