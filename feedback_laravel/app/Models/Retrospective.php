<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Http\Request;

class Retrospective extends Model
{
    use HasFactory, HasUuids;

    protected $hidden = ['created_at', 'updated_at'];
    public $timestamps = false;
    

    /**
     *  
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'titre',
        'description',

    ];

    //indique à pluck que le type est string et non int
    protected $casts = [
        'id' => 'string',
    ];
    //Relation entre les différentes tables


    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function feedback(): HasMany
    {
        return $this->hasMany(Feedback::class, 'retrospective_id');
    }

    public function participant(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'participants', 'retrospective_id', 'user_id');
    }
};