<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;


    public $timestamps = false;
    //Relation entre les différentes tables

    public function retrospective(): HasMany
    {
        return $this->hasMany(Retrospective::class, 'user_id');
    }

    public function participant(): BelongsToMany
    {
    return $this->belongsToMany(Retrospective::class, 'participants', 'user_id', 'retrospective_id');
    }

    

    /**
     *  
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'prenom',
        'email',
        'password',
    ];

    /**
     *  Permet de ne pas prendre en compte les champs ci dessous

     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'created_at', 
        'updated_at',
    ];

    /**
     *  
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
