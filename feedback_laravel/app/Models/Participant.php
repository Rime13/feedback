<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    
    use HasFactory;
    //Permet de ne pas prendre en compte les champs created_at et updated_at
    protected $hidden = ['created_at', 'updated_at'];
    public $timestamps = false;
}
