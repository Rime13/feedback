# Application de gestion de rétrospective

Un projet m'a été fourni par mes formateurs, le but était de créer une application de retrospective anonyme.

Brief : https://gitlab.com/Glandalf/cda-2023/-/tree/main/projet-intermediaire


---
## Sommaire  <!-- omit from toc --> 


- [Application de gestion de rétrospective](#application-de-gestion-de-rétrospective)
  - [Résumé de l'application](#résumé-de-lapplication)
  - [Conception](#conception)
    - [Méthode UML](#méthode-uml)
      - [Diagramme de cas d'utilisation](#diagramme-de-cas-dutilisation)
      - [Diagramme d'activité](#diagramme-dactivité)
    - [Méthode Merise](#méthode-merise)
      - [Modèle conceptuel de données](#modèle-conceptuel-de-données)
      - [Modèle logique de données](#modèle-logique-de-données)
  - [Développment](#développment)
    - [Technologies utilisées](#technologies-utilisées)
    - [Pré-requis](#pré-requis)




---

## Résumé de l'application

**Le but :**  Permettre à un utilisateur de recueillir des retours de participants de manière anonyme via une application.

Un utilisateur anonyme devra se connecter sur l'application afin de pouvoir créer une rétrospective ou bien y participer.

N'importe quel utilisateur peut créer une rétrospective, son créateur sera automatiquement l'administrateur de sa rétrospective et pourra inviter d'autres utilisateurs à y participer via un lien.

Chaque rétrospective sera réinitialisé tous les 7 jours automatiquement afin de pouvoir effectuer un suivi quotidien.

Les feedbacks seront publiés de manière anonyme ni l'admin ni les autres participants pourront voir qui les a publiés.

**L'administrateur**, le créateur de la rétrospective, aura un accès aux différents feedbacks des utilisateurs de manière anonyme et ceux qui n'ont pas participer.

Il faudra **un minimum de 30% de participation** afin d'accéder à la liste des non participants.

Le créateur pourra accéder aux précédentes rétrospectives à travers un historique.

L'administrateur peut consulter les feedbacks sans les avoir publiés.


**L'utilisateur**, le participant, pourra renseigner un ou plusieurs feedbacks et pourra consulter ceux des autres participants sans savoir qui les a publiés.

Avant d'avoir accès aux différents feedbacks déjà publiés, il devra avoir renseigné **au moins un feedback dans la catégorie "J'aime" et un dans la catégorie "Je n'aime pas"**.

---
## Conception

### Méthode UML

#### Diagramme de cas d'utilisation

**Un utilisateur** pourra créer un feedback, il devra renseigné au moins un j'aime et ne j'aime pas pour accéder aux différents feedbacks.

**L'administrateur**  pourra quant à lui créer et supprimer une ou plusieurs rétrospectives.
Il pourra lister les différentes rétrospectives ou celle actuelle à l'aide de filtre.
Une fois la rétrospective sélectionnée, l'administrateur pourra accéder à la liste des non-participants.
![Diagramme UseCase](UML/useCase.svg)

---

#### Diagramme d'activité

**Création de rétrospective de la part d'un admin.**

Ce diagramme montre le trajet possible de la part d'un admin lors de la création d'une rétrospective.

Il y aura deux cas possibles si des rétrospectives sont déjà existantes ou si ce n'est pas le cas.
![Diagramme d'activité retrospective](UML/activite_retrospective.svg)

**Création de feedback de la part d'un utilisateur.**

Ce diagramme expose la création d'un feedback de la part d'un utilisateur participant avec deux cas possibles.
S'il a déjà publié des feedbacks ou s'il n'en a pas publiés.
![Diagramme d'activité feedback](UML/activite_creation_feedback.svg)

---

### Méthode Merise

#### Modèle conceptuel de données

Ce modèle possède plusieurs **entités** :

- Utilisateur
- Rétrospective
- Feedback
  
Il possède aussi plusieurs **associations**:

- **Participent** qui lie les deux entités utilisateur et rétrospective.
- **Créer** qui lie utilisateur et rétrospective.
- **Appartient** qui lie rétrospective et feedback.


**Participent :**  Un utilisateur peut participer à zéro ou à plusieurs rétrospectives.
Une rétrospective peut contenir une participation de zéro ou plusieurs utilisateurs.

**Créer :** N'importe quel utilisateur peut créer une rétrospective, il en deviendra l'administrateur.
Une rétrospective peut être créé par un seul utilisateur.

**Appartient :** Un feedback appartient à une rétrospective.
Une rétrospective peut être composé de zéro ou plusieurs feedbacks.

![Modèle conceptuel de données](MERISE/MCD.svg)

---
#### Modèle logique de données

Pour établir le Modèle logique de données, j'ai transformé chaque entité de mon MCD en une table correspondante dans le MLD. J'ai ensuite identifié les attributs pour chaque table et les ai ajoutés.

Ensuite, j'ai identifié les clés primaires pour chaque table. Chaque table doit avoir une clé primaire qui identifie de manière unique chaque enregistrement de la table. J'ai créé des clés primaires simples.

Ensuite, j'ai traduit les relations entre les entités du MCD en relations entre les tables dans le MLD. 

![Modèle logique de données](MERISE/MLD.png)

---

## Développment

### Technologies utilisées

- Back : Laravel
- Front : VueJS
- BDD : MariaDB


### Pré-requis

- Installer Laravel
- Installer VueJS
- Installer la mariadb 
---

Une fois les technologies installées, il faudra crée la BDD

Création de la base de données :


- Ouvrir un terminal de commande
- Démarrer la BDD avec la commande : sudo mysql
- Crée une BDD avec le nom retro avec la commande : create databases retro
- Se placer dans la BDD avec la commande 
- Créer un utilisateur afin de pouvoir accéder à la BDD avec la commande :  CREATE USER if NOT EXISTS 'userRetro'@'localhost' IDENTIFIED BY 'password';
- Donner les droits à l'utilisateur via cette commande : GRANT ALL PRIVILEGES ON retro.* TO 'userRetro'@'localhost';


Une fois que vous avez créer la base de données ainsi que l'utilisateur, vous devez renseigner les identifiants de connexion dans le projet laravel.

---

Connexion à la BDD :

- Ouvrir un terminal et se rendre dans le projet laravel
- Se rendre dans le fichier .env s'il nexiste pas dupliquer le fichier .env.exemple et conserver uniquement .env
- Chercher la section ci dessous

![Connexion BDD](img/Capture%20d%E2%80%99%C3%A9cran%20du%202023-04-14%2023-28-01.png)

- Renseignez les memes informations présentes sur la photo

Le projet est à présent relier à la BDD.

---

Une fois la BDD créé, il faudra créé les tables.

Pour cela il faudra ouvrir un terminal et se placer dans le projet laravel, il faudra ensuite éxécuter les étapes suivantes.


- Il faudra exécuter les migrations, cela permettra de créé les différentes tables avec la commande : php artisan migrate

- Une fois la migration terminée il faudra créé les données tests avec la commande suivante : php artisan db:seed

Ceci permettra de vérifier que les données se sont bien créés dans les tables qui leurs sont destinées.

---

Lorsque tout sera installer il faudra lancer les deux serveurs laravel et vuejs afin de pouvoir acceder à l'application.

- Lancer le serveur dans un terminal avec la commande : php artisan serve
- Lancer le serveur dans un autre terminal avec la commande : npm run dev 

Il faudra se rendre sur l'adresse du serveur laravel afin d'acceder à l'application.
